import "es6-shim";
import "reflect-metadata";
import "@angular/platform-browser";
import "@angular/core";
import "@angular/router-deprecated";
import "@angular/http";
import "rxjs";
require('zone.js/dist/zone');


// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...

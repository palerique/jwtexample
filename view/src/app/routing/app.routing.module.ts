import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {UsersComponent} from "../users/users.component";
import {LoginComponent} from "../login/login.component";

const routes = [
  {
    path: 'home',
    redirectTo: "/"
  },
  {
    path: 'users',
    component: UsersComponent
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

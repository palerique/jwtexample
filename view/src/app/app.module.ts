import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component";
import {AlertModule} from "ng2-bootstrap";
import {UsersComponent} from "./users/users.component";
import {LoginComponent} from "./login/login.component";
import {AppRoutingModule} from "./routing/app.routing.module";
import {AuthenticationService} from "./services/authentication-service.service";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AlertModule.forRoot(),
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    UsersComponent,
    LoginComponent
  ],
  providers: [
    AuthenticationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

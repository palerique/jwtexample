"use strict";
/* tslint:disable:no-unused-variable */
var testing_1 = require("@angular/core/testing");
var authentication_service_service_1 = require(
  "./authentication-service.service");
describe('AuthenticationServiceService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [authentication_service_service_1.AuthenticationServiceService]
        });
    });
    it('should ...', testing_1.inject([authentication_service_service_1.AuthenticationServiceService], function (service) {
        expect(service).toBeTruthy();
    }));
});

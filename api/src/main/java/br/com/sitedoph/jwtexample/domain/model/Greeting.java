package br.com.sitedoph.jwtexample.domain.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Greeting {

    private final long id;
    private final String content;

}
